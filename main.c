#define STM32F103x6
#include <stm32f1xx.h>
#include <stdint.h>
#define DELAY 3300

void SystemInit() {
  SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPAEN);
}

int main() {
  MODIFY_REG(
             GPIOA->CRL,
             GPIO_CRL_MODE0 | GPIO_CRL_CNF0,
             GPIO_CRL_MODE0_1
             );
  SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS0);

  while (1) {
    for (int i = 0; i < DELAY; i++) {
      asm("nop");
    }
    SET_BIT(GPIOA->BSRR, GPIO_BSRR_BR0);
    for (int i = 0; i < DELAY; i++) {
      asm("nop");
    }
    SET_BIT(GPIOA->BSRR, GPIO_BSRR_BS0);
  }
}

void _exit(int _) {while(1) {}}
